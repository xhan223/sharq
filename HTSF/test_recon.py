from preprocess.hierarchical import TreeNodes
import numpy as np
from numpy.linalg import inv
import pdb


def get_p_matrix(S, Y, Y_hat):
    P = -np.dot(np.dot(inv(np.dot(S.T, S)), np.dot(S.T, Y.T)), np.dot(Y_hat, inv(np.dot(Y_hat.T, Y_hat))))
    return P


def ERM(nodes, y, y_hat):
    S = TreeNodes(nodes).get_s_matrix()
    P = get_p_matrix(S, y, y_hat)
    result = []
    for i in range(y_hat.shape[0]):
        recon_pred = {}
        y_tilde = np.dot(np.dot(S, P), y_hat[i, :])
        for i, e in enumerate(y_tilde):
            recon_pred[str(i)] = e
        if y_hat.shape[0] == 1:
            return recon_pred
        result.append(recon_pred)
    return result


def consistency_error(parent, childs, test_pred):
    '''
    Calculate the absolute difference between parent and child forecasts.
    :param parent: name of parent node
    :param childs: name of child nodes
    :param test_pred: forecasting results for each node
    :return: consistency error for a single parent-childs group
    '''
    child_pred = 0
    for child in childs:
        child_pred += test_pred[child]
    consistency_error = np.absolute(test_pred[parent] - child_pred)
    return consistency_error


def get_consistency(nodes, node_list, test_pred):
    total_error = 0
    for name in node_list:
        node = TreeNodes(nodes, name=name)
        if node.get_child() is not None:
            total_error += consistency_error(name, node.get_child()[1:], test_pred)
    return total_error


def get_mape_loss(node_list, nodes, pred, test, l):
    level_loss = dict(zip(range(l), np.zeros(l)))
    for name in node_list:
        node = TreeNodes(nodes, name=name)
        loss = 100 * np.absolute(np.array(test[int(name)] - pred[name])) / np.absolute(test[int(name)])
        level_loss[node.get_levels()] += loss
    for l in range(1, len(level_loss) + 1):
        level_loss[l - 1] = level_loss[l - 1] / len(TreeNodes(nodes).nodes_by_level(l))
    return np.fromiter(level_loss.values(), dtype=float)


if __name__ == '__main__':

    nodes = [[10], [20, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    S = TreeNodes(nodes).get_s_matrix()
    y_bot = 10 * np.random.sample(size=S.shape[1]) + 10
    y = np.dot(S, y_bot)
    y_hat = y + np.random.normal(loc=1, scale=3, size=S.shape[0])

    node_list = [str(i) for i in range(len(y))]
    pred = dict(zip(node_list, y_hat))
    true = dict(zip(node_list, y))
    coherency_error = get_consistency(nodes, node_list, pred)
    print('Prediction coherency error: ', coherency_error)
    y_recon = ERM(nodes, np.expand_dims(y, axis=0), np.expand_dims(y_hat, axis=0))
    print('Reconciled coherency error: ', get_consistency(nodes, node_list, y_recon))

    print('\n')
    mape_pred = get_mape_loss(node_list, nodes, pred, y, len(nodes) + 1)
    mape_reconciled = get_mape_loss(node_list, nodes, y_recon, y, len(nodes) + 1)
    print('Prediction MAPE: ', mape_pred)
    print('Reconciled MAPE ', mape_reconciled)
