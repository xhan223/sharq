import numpy as np
import pandas as pd
from itertools import chain
from tqdm import tqdm
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage
from rpy2.robjects.packages import importr
from preprocess import r_snippet
import rpy2.robjects as ro
import rpy2.robjects.numpy2ri
import pdb


def recon_base_forecast(node_list, nodes, median_forecast, models, train, h, recon, alg):
    pred = np.zeros((1, sum(list(chain(*nodes))) + 1))
    for name in node_list:
        pred[0, int(name)] = median_forecast[name]

    rpy2.robjects.numpy2ri.activate()
    nr, nc = pred.shape
    pred_r = ro.r.matrix(pred, nrow=nr, ncol=nc)

    feed_dict = {}
    for i, e in enumerate(nodes):
        if len(e) == 1:
            feed_dict['Level ' + str(i)] = e[0]
        else:
            feed_dict['Level ' + str(i)] = ro.IntVector(e)
    node_structure = ro.ListVector(feed_dict)

    if not isinstance(train, pd.DataFrame):
        training = int(train.tr * train.n)
        train_np = train.dat[:training, :]
        residual = np.zeros_like(train_np)
        residual[:h] = train_np[:h]
    else:
        residual = np.zeros_like(train.values)
        residual[:h] = train.values[:h]
    residual = insample_residual(node_list, models, train, residual, h, alg, recon)
    nr, nc = residual.shape
    residual_r = ro.r.matrix(residual, nrow=nr, ncol=nc)

    hts = importr('hts')
    MinT = hts.MinT
    combinef = hts.combinef
    if recon == 'mint_shr':
        mint = r_snippet.mint_shr()
    elif recon == 'mint_sam':
        mint = r_snippet.mint_sam()
    else:
        mint = r_snippet.mint_ols()

    try:
        powerpack = SignatureTranslatedAnonymousPackage(mint, "powerpack")
        recon_pred = powerpack.mint_recon(pred_r, node_structure, residual_r)
    except:
        print('MinT does not apply here as covariance matrix is not positive definite.')
        return median_forecast

    recon_pred = np.array(recon_pred)
    median_forecast_recon = {}
    for i, e in enumerate(recon_pred[0]):
        median_forecast_recon[str(i)] = e
    return median_forecast_recon


def insample_residual(node_list, models, train, residual, h, alg, recon):
    print('Computing in-sample residual... \n')
    if alg == 'rnn' or recon == 'sharq':  # need to think about the data input format for lstnet with sharq
        for i in range(h + 1, train.values.shape[0]):
            for name in tqdm(node_list):
                X = models[name].get_x_y(np.array(train[name])[:i])
                residual[i, int(name)] = train.values[i, int(name)] - models[name](X)
    else:
        residual[h:, :] = train.dat[h:int(train.tr * train.n), :] - models(train.train[0].cuda()).cpu().detach().numpy()
    return residual
