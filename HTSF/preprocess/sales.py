import numpy as np
import pandas as pd
import datetime


def sales_preprocess():
    sales = pd.read_csv('/Users/hanxing/Desktop/HTSF data/sales_train.csv')
    item_cat = pd.read_csv('/Users/hanxing/Desktop/HTSF data/item_categories.csv')
    item = pd.read_csv('/Users/hanxing/Desktop/HTSF data/items.csv')
    shop = pd.read_csv('/Users/hanxing/Desktop/HTSF data/shops.csv')
    test = pd.read_csv('/Users/hanxing/Desktop/HTSF data/test.csv')

    sales.date = sales.date.apply(lambda x: datetime.datetime.strptime(x, '%d.%m.%Y'))

    ids = np.sort(sales['item_id'].unique())
    total_records, store_nums = {}, {}
    for i in ids:
        single_id = sales[sales['item_id'] == i]
        total_record, store_num = single_id.shape[0], len(single_id['shop_id'].unique())
        total_records[i], store_nums[i] = total_record, store_num

    # Top 5 items with most records
    top_items = sorted(total_records, key=total_records.get, reverse=True)[:5]
    for i in top_items:
        print('item {} has {} records and distrubuted in {} stores'.format(i, total_records[i], store_nums[i]))

    single_id = sales[sales['item_id'] == 20949]
    single_id = single_id.sort_values(by=['date'])
    y = single_id.groupby(['date'])['item_cnt_day'].sum()
    d = {'date': y.index, 'item_cnt_day': list(y)}
    y = pd.DataFrame(data=d)
    shop_id = sales['shop_id'].unique()

    for i in shop_id:
        single_shop = single_id[single_id['shop_id'] == i][['date', 'item_cnt_day']]
        y = y.merge(single_shop, how='left', on='date', suffixes=('', '_' + str(i)))
    y = y.fillna(0)
    y.to_csv('./hierarchical_data.csv')
