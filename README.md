# Hierarchical Time Series Forecasting

This repository is an implementation of the KDD'20 submission **Simultaneously Reconciled Quantile Forecasting of Hierarchically Related Time Series**, 
along with other hierarchical time series (hts) forecasting baselines by [Rob Hyndman](https://robjhyndman.com/).

## Synopsis

### Requirements
The following environment and packages are required to run the program:
- Linux with Python 3.6+.
- OS X with Python 3.6+.
- Pytorch 1.0+.
- Numpy
- Pandas
- rpy2
- matplotlib
- tensorboardX
- tqdm

### Installation

#### Install HTSF using pip
The complete package will be available soon.

#### Create running environment
To setup conda environment for running the program, use the following command:
```
conda env create -f htsf.yml
```
Activate the new environment:
```
conda activate htsf
```
Verify that the new environment was installed correctly:
```
conda env list
```

## Program Details
The program compares forecasting performance across benchmarked hts algorithms on various real-world and simulated hierarchiclly related time series data.
The data set contains both temporal and cross-sectional hierarchies.
### Data set
- [Australian Labour Force](https://www.abs.gov.au/ausstats/abs@.nsf/mf/6202.0)
- [Predict Future Sales](https://www.kaggle.com/c/competitive-data-science-predict-future-sales/data)
- [Web Traffic Time Series Forecasting](https://www.kaggle.com/c/web-traffic-time-series-forecasting)
- [M3 Competition Data](https://forecasters.org/resources/time-series-data/m3-competition/)
- [AEdemand Data](https://cran.r-project.org/web/packages/thief/thief.pdf)

### Forecasting Algorithms
- Recurrent Neural Networks (RNN)
- Long Short Term Memory networks (LSTM)
- Auto-regressive Models (AR)

### Reconciliation Methods
- Bottom up (BU) method.
- [Trace Minimization (MinT)](https://robjhyndman.com/papers/MinT.pdf) including shrinkage, sampling and OLS estimators.
- [Empirical risk minimization (ERM)](http://souhaib-bentaieb.com/pdf/2019_KDD.pdf).
- [hts prophet](https://github.com/CollinRooney12/htsprophet).
- SHARQ (our method).

### Program Structure
- [`test.py`](./HTSF/test.py): script to run the sharq algorithm along with other models.
- [`sharq.py`](./HTSF/sharq.py): the wrapper of hts algorithms and data sets.
- [`algorithms`](./HTSF/algorithms): implementation of the list of forecasting algorithms and reconciliation methods.
- [`data`](./HTSF/data): hierarchical time series data sets.
- [`preprocess`](./HTSF/preprocess): preprocess raw time series data from the web, define hierarchical graph structure, etc.
- [`evaluation`](./HTSF/evaluation): evaluation metrics and visualization for out of sample forecasting.


## Running the Program
Please refer to [`running_instructions.pdf`](./running_instructions.pdf) for how to run the program.

The program will generate results in CSV format under `./results/` directory. For the point forecast, results contain forecasting accuracy,
reconciliation error, and program run time (training + inference) comparison for each hts algorithm. For distributional forecasts, the program
will generate the forecasting likelihood ratio, and corresponding forecasting plots (see example below).
<img src="./HTSF/figure/sim_small_forecast.png" width=300>
## Troubleshooting
If you have further questions on implementation details, please contact aaronhan223@utexas.edu